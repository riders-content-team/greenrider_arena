#include "ContactPlugin.hh"

using namespace gazebo;
GZ_REGISTER_SENSOR_PLUGIN(ContactPlugin)

std_msgs::Bool belt_msg;

/////////////////////////////////////////////////
ContactPlugin::ContactPlugin() : SensorPlugin(){
  this->belt_state = false;
}

/////////////////////////////////////////////////
ContactPlugin::~ContactPlugin(){
}

/////////////////////////////////////////////////
void ContactPlugin::Load(sensors::SensorPtr _sensor, sdf::ElementPtr _sdf){
  // Get the parent sensor.
  this->parentSensor = std::dynamic_pointer_cast<sensors::ContactSensor>(_sensor);
  this->world = physics::get_world(this->parentSensor->WorldName());
  // Make sure the parent sensor is valid.
  if (!this->parentSensor){
    std::cout << "ContactPlugin requires a ContactSensor.\n";
    return;
  }
  // Connect to the sensor update event.
  this->updateConnection = this->parentSensor->ConnectUpdated(std::bind(&ContactPlugin::OnUpdate, this));
  // Make sure the parent sensor is active.
  this->parentSensor->SetActive(true);
  this->rosNode.reset(new ros::NodeHandle());

  ros::SubscribeOptions so =
    ros::SubscribeOptions::create<std_msgs::Bool>(
      "belt_state",
      1,
      std::bind(&ContactPlugin::BeltStateCallback, this, std::placeholders::_1),
      ros::VoidConstPtr(),
      NULL);

  this->belt_state_sub = this->rosNode->subscribe(so);
}

void ContactPlugin::BeltStateCallback(const std_msgs::Bool::ConstPtr& msg)
{
  this->belt_state = msg->data;
}

void ContactPlugin::OnUpdate(){
  // Get all the contacts.
  if(this->belt_state){
    this->GetContactModels();
    for (int i=0; i<this->models_on_belt.size(); i++)
    {
      physics::ModelPtr belt_model;
      belt_model = this->world->ModelByName(this->models_on_belt[i]);
      belt_model->SetLinearVel(ignition::math::Vector3d(0.4,0,0));
    }
  }
}

void ContactPlugin::GetContactModels(){
  msgs::Contacts contacts;
  contacts = this->parentSensor->Contacts();
  this->models_on_belt.clear();
  for (int i = 0; i < contacts.contact_size(); ++i){
    std::string touching_model = contacts.contact(
       i).collision1().substr(0, contacts.contact(
         i).collision1().find("::"));
    //std::cout << touching_model << '\n';

    this->models_on_belt.push_back(touching_model);
  }
}
