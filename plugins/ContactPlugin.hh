#ifndef _GAZEBO_CONTACT_PLUGIN_HH_
#define _GAZEBO_CONTACT_PLUGIN_HH_

#include <string>

#include <gazebo/gazebo.hh>
#include <gazebo/sensors/sensors.hh>

#include "ros/ros.h"
#include "std_msgs/Bool.h"

namespace gazebo
{
  //An example plugin for a contact sensor.
  class ContactPlugin : public SensorPlugin
  {
    //Constructor.
    public: ContactPlugin();

    // Destructor.
    public: virtual ~ContactPlugin();

    //Load the sensor plugin.
    // _sensor Pointer to the sensor that loaded this plugin.
    // _sdf SDF element that describes the plugin.

    public: virtual void Load(sensors::SensorPtr _sensor, sdf::ElementPtr _sdf);

    //Callback that receives the contact sensor's update signal.
    private: virtual void OnUpdate();

    private: virtual void GetContactModels();

    public: virtual void BeltStateCallback(const std_msgs::Bool::ConstPtr& msg);

    // Pointer to the contact sensor
    private: sensors::ContactSensorPtr parentSensor;

    //Connection that maintains a link between the contact sensor's
    // updated signal and the OnUpdate callback.
    private: event::ConnectionPtr updateConnection;

    //World in which the contact occurred
    public: physics::WorldPtr world;
    // Pointer to the Model
    //ROS Node Handler
    private: std::unique_ptr<ros::NodeHandle> rosNode;
    //ROS publisher
    private: ros::Subscriber belt_state_sub;

    private: bool belt_state;

    private: std::vector<std::string> models_on_belt;

  };
}
#endif
